## Pass "pipeline flag" between steps, making silent failure possible ( not running subsequent job scripts )

#### Created in order to exemplify workaround for silent pipeline failure / not running subsequent job scripts.

https://gitlab.com/gitlab-org/gitlab/-/issues/224772#note_369284933
